<?php namespace core\base;

use Core;

class Antispam
{
	public $freq = 60; //300 секунд = 5 минут

	public function action()
	{
		Core::log('Запуск модуля Антиспам');
		$result = Core::$app->api->newsInbox();
		// Парсим результат - получаем массив с комментами
		if(!defined('REG_STOPWORDS')){
			define('REG_STOPWORDS', "#тестантиспама#iu");
		}

		$delete = [];
		$targetIndexes = ['new_stories', 'old_stories'];
		foreach ($targetIndexes as $targetIndex) {
			foreach ($result[$targetIndex] as $action) {
				if ($action['type'] === 1) {
					if (strpos($action['args']['text'], 'commented')) {
						if (preg_match(REG_STOPWORDS, $action['args']['text'])) {
							$i = count($delete);
							$delete[$i]['text'] = $action['args']['text'];
							$delete[$i]['media_id'] = $action['args']['media']['0']['id'];
							$delete[$i]['commentator_id'] = $action['args']['profile_id'];
							// Получаем id коммента
							$media = Core::$app->api->mediaComments($delete[$i]['media_id']);
							foreach ($media['comments'] as $comment) {
								if (strpos($delete[$i]['text'], $comment['text'])) {
									$delete[$i]['comment_id'] = $comment['pk'];
									// Пишем логи
									Core::log('Удаление комментария ' . $delete[$i]['text']);

									// Выдёргиваем ссылку на Media
									$media = Core::$app->api->mediaInfo($delete[$i]['media_id']);
									$media_code = $media['items'][0]['code'];

									$query = sprintf('INSERT INTO antispam_log
									(media_id, comment_id, text, user_id, account_id, commentator_login, commentator_id, media_code) VALUES ("%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s")',
										$delete[$i]['media_id'],
										$delete[$i]['comment_id'],
										mb_substr($delete[$i]['text'], mb_strpos($delete[$i]['text'], 'commented:') + 11),
										Core::$settings['user_id'],
										Core::$settings['account_id'],
										mb_substr($delete[$i]['text'], 0, mb_strpos($delete[$i]['text'], 'commented:')-1),
										$delete[$i]['commentator_id'],
										$media_code
									);
									//Core::log($query);

									Core::$db->query($query);
									// Удаляем комментарии
									Core::$app->api->mediaCommentDelete($delete[$i]['media_id'], $delete[$i]['comment_id']);
								}
							}
						}
					}
				}
			}
		}
	}
}