<?php namespace core\base;

use Core;
use core\instagram\exceptions\ParamsException;

class Model implements \JsonSerializable
{
	protected $_attributes = [];
	protected $_required_attribures = [];

	public function __construct($data = null)
	{
		$this->load($data);
	}

	/**
	 * @return string
	 */
	public static function className()
	{
		return get_called_class();
	}

	/**
	 * @return array
	 */
	public static function classes()
	{
		return [];
	}

	/**
	 * @param array $data
	 * @throws ParamsException
	 */
	public function load($data)
	{
		if (!is_array($data)) {
			return;
		}

		foreach ($data as $name => $value) {
			if (isset($this::classes()[$name])) {
				$class_name = $this::classes()[$name];
				$this->$name = new $class_name($value);
				continue;
			}

			$this->$name = $value;
		}
		foreach ($this->_required_attribures as $required_attribure) {
			if (!isset($this->$required_attribure) && is_null($this->$required_attribure) && is_null($this->_attributes[$required_attribure])){
				throw new ParamsException($required_attribure.' is not set');
			}
		}
	}

	public function loadFromFile($file_name)
	{
		$path = Core::$app->work_dir.DIRECTORY_SEPARATOR.$file_name;
		if (!file_exists($path)) {
			return false;
		}

		$data = json_decode(file_get_contents($path), true);
		if (is_null($data)) {
			Core::log('Parse JSON error, bad data in "'.$file_name.'"');
			return false;
		}

		try {
			$this->load($data);
		} catch (ParamsException $e){
			return false;
		}
		return true;
	}

	public function saveTo($file_name)
	{
		$this->saveToFile($file_name);
	}

	public function saveToFile($file_name)
	{
		$path = Core::$app->work_dir.DIRECTORY_SEPARATOR.$file_name;
		file_put_contents($path, json_encode($this));
	}

	public function __get($name)
	{
		if (array_key_exists($name, $this->_attributes)) {
			return $this->_attributes[$name];
		}

		$getter = 'get'.ucfirst($name);
		if (!method_exists($this, $getter)) {
			throw new \Exception('There is no getter for "'.$name.'" in '.self::className());
		}
		return $this->$getter();
	}

	public function __set($name, $value)
	{
		if (array_key_exists($name, $this->_attributes)) {
			$this->_attributes[$name] = $value;
			return;
		}

		$setter = 'set'.ucfirst($name);
		if (method_exists($this, $setter)) {
			$this->$setter($value);
		} else {
			throw new \Exception('There is no setter for "'.$name.'" in '.self::className());
		}
		return;
	}

	/**
	 * Specify data which should be serialized to JSON
	 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	function jsonSerialize()
	{
		return $this->_attributes;
	}
}