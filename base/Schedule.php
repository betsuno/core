<?php namespace core\base;
use Core;
class Schedule
{
	private $_tasks = [];
	public function addCallback($tag, $callback, $freq, $next_start = 0){
		$this->_tasks[$tag] = [
			'callback' => $callback,
			'freq' => $freq,
			'next_start' => $next_start
		];
	}

	public function removeCallback($tag){
		unset($this->_tasks[$tag]);
	}
	public function run(){
		foreach ($this->_tasks as $tag => $task){
			//Core::log("Проверяем таймер $tag");
			if ($task['next_start'] <= time()) {
				//Core::log('Текущее время ' . date('H:i:s'));
				//Core::log('Целевое время запуска таймера ' . date('H:i:s', $task['next_start']));
				//Core::log("Частота запуска $task[freq] секунд");
				//Core::log("Запускаем $tag");
				call_user_func($task['callback']);
				$this->_tasks[$tag]['next_start'] = time() + $task['freq'];
				break; // Ораничеваемся одним действием
			}
		}
	}
}