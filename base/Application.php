<?php namespace core\base;

use core\instagram\Api;

/**
 * Class Application
 * @package core
 *
 * @property string $work_dir
 * @property string captcha_apikey
 * @property Api $api
 */
class Application extends Model
{
	protected $_attributes = [
		'work_dir'       => __DIR__,
		'api'            => null,
		'captcha_apikey' => null,
	];
	public $schedule;

	public static function classes()
	{
		return [
			'api' => Api::className(),
		];
	}
}