<?php

spl_autoload_register(function($class){
	$class = str_replace('\\', DIRECTORY_SEPARATOR, $class);
	$class = rtrim($class, DIRECTORY_SEPARATOR);
	$class = str_replace('core'.DIRECTORY_SEPARATOR, '', $class);
	$filename = __DIR__.DIRECTORY_SEPARATOR.$class.'.php';
	if (file_exists($filename)) {
		/** @noinspection PhpIncludeInspection */
		require_once($filename);
	} else {
		throw new Exception('Class not found "'.$class.'"');
	}
}, true, true);

include __DIR__.DIRECTORY_SEPARATOR.'Core.php';
