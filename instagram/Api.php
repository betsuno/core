<?php namespace core\instagram;

use core\instagram\api\Api_v_7_11_1;
use core\instagram\api\Api_v_7_9_0;

// Это хелпер, наследует последнюю версию, нужен для упрощения кода
class Api extends Api_v_7_9_0 {}