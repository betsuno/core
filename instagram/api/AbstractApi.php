<?php namespace core\instagram\api;

use Core;
use core\base\Model;
use core\instagram\Device;
use core\instagram\exceptions\CheckpointReqiredException;
use core\instagram\exceptions\ConnectionException;
use core\instagram\exceptions\FeedbackReqiredException;
use core\instagram\exceptions\LoginReqiredException;
use core\instagram\exceptions\ParamsException;
use core\instagram\exceptions\ParseException;
use core\instagram\exceptions\ResponseException;
use core\instagram\exceptions\ServerErrorException;
use core\instagram\exceptions\TooManyRequestsException;

/**
 * @property string  $username
 * @property string  $password
 * @property string  $user_id
 * @property boolean $logged_in
 * @property string  $proxy
 * @property string  $ip
 * @property string  $userAgent
 * @property Device  $device
 */
abstract class AbstractApi extends Model
{
	const API_URL     = 'https://i.instagram.com/api/v1/';
	const API_SECRET  = '';
	const API_VERSION = '';

	protected $_curl_handler;
	protected $_last_response;
	protected $_csrf_token;
	protected $_parse_response = true;
	protected $_cookieFile;

	protected $_attributes = [
		'username'  => null,
		'password'  => null,
		'logged_in' => false,
		'user_id'   => null,
		'proxy'     => null,
		'ip'        => null,
		'device'    => null,
		'phone_id'  => null,
	];

	/** @var Device $_device */
	protected $_device;

	protected $verbose = false;

	//captcha fields
	protected $referrer;
	protected $additionalHeaders;

	abstract protected function accountsLogin();
	abstract protected function accountsLogout();

	abstract protected function qeExpose($data);
	abstract protected function pushRegister($data);

	abstract protected function directShareRecentRecipients($data);
	abstract protected function directV2Inbox($data);

	abstract protected function feedUser($user_id, $max_id = null);
	abstract protected function feedTimeline($max_id = null);
	abstract protected function feedLocation($location_id, $count = 100, $max_id = null);

	abstract protected function news();
	abstract protected function newsInbox();

	abstract protected function uploadPhoto($file_name);
//	abstract protected function mediaConfigure($file_name, $upload_id, $comment, $media_info);

	abstract protected function mediaLike($media_id);

	abstract protected function friendshipShow($insta_id);
	abstract protected function friendshipFollowing($insta_id);
	abstract protected function friendshipsAutocompleteUserList();

	abstract protected function usersInfo($insta_id);
	abstract protected function usersSearch($query);

	abstract protected function fbsearchPlaces($insta_id, $location, $limit = 20, $lat = null, $lng = null);

	abstract protected function discoverPopular();

	abstract public function recognizeCaptcha($url);

	abstract public function login($username, $password);
	abstract public function logout();
	abstract public function postImage($file_name, $comment = null);

	public static function classes()
	{
		return [
			'device' => Device::className()
		];
	}

	/**
	 * Initializes cURL handler and sets options
	 */
	protected function initCurl()
	{
		$this->_curl_handler = curl_init();
		$this->_cookieFile = \Core::$app->work_dir.sprintf('cookies_%s_%s.txt', $this->username, $this::API_VERSION);
		$this->curlSetHeaders();
		curl_setopt_array($this->_curl_handler, [
			CURLOPT_USERAGENT      => $this->getUserAgent(),
			CURLOPT_COOKIEFILE     => $this->_cookieFile,
			CURLOPT_COOKIEJAR      => $this->_cookieFile,
			CURLOPT_ENCODING       => 'gzip',
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HEADER         => true
		]);
		if (!is_null($this->proxy)) {
			curl_setopt($this->_curl_handler, CURLOPT_PROXY, $this->proxy);
		}
	}

	/**
	 * Sets headers for cURL session
	 */
	protected function curlSetHeaders()
	{
		$headers = [
//			'X-Google-AD-ID: 055626e2-8913-49c7-a3d2-133f1a92eff9',
//			'X-IG-INSTALLED-APPS: eyIxIjowLCIyIjowfQ==',
			'X-IG-Connection-Type' => 'WIFI',
			'X-IG-Capabilities' => 'HQ==',
			'Connection' => 'Keep-Alive',
			'Accept' => 'application/json',
			'Cookie2' => '$Version=1',
			'Accept-Language' => 'en-US',
			'Accept-Encoding' => 'gzip',
		];
		if ($this->ip !== null) {
			$headers['HTTP_X_FORWARDED_FOR'] = $this->ip;
			$headers['X_FORWARDED_FOR'] = $this->ip;
			$headers['HTTP_CLIENT_IP'] = $this->ip;
			$headers['CLIENT_IP'] = $this->ip;
		}
		if (!empty($this->additionalHeaders)) {
			$headers = array_merge($headers, $this->additionalHeaders);
			$this->additionalHeaders = [];
		}
		$headersArray = [];
		foreach ($headers as $key => $header) {
			$headersArray[] = $key.': '. $header;
		}
		curl_setopt($this->_curl_handler, CURLOPT_HTTPHEADER, $headersArray);
	}

	/**
	 * Makes request to instagram server with specified data
	 *
	 * @param string $uri
	 * @param array|null $data
	 * @param boolean $sign
	 *
	 * @throws CheckpointReqiredException
	 * @throws ConnectionException
	 * @throws FeedbackReqiredException
	 * @throws LoginReqiredException
	 * @throws ParseException
	 * @throws ResponseException
	 * @throws TooManyRequestsException
	 * @throws null
	 */
	protected function requestRaw($uri, $data = null, $sign = true){
		if (is_null($this->_curl_handler)) {
			$this->initCurl();
		}
		if (!(is_null($this->referrer))) {
			curl_setopt($this->_curl_handler, CURLOPT_REFERER, $this->referrer);
			$this->referrer = null;
		}
		$try = 0;
		do {
			curl_setopt_array($this->_curl_handler, [
				CURLOPT_URL         => $uri,
				CURLOPT_POST        => $data !== null,
				CURLINFO_HEADER_OUT => true,
			]);

			if (!is_null($data)) {
				if (!empty($this->_csrf_token)) {
					if (is_array($data)) {
						$data['_csrftoken'] = $this->_csrf_token;
					} else {
						$data .= '&_csrftoken=' . $this->_csrf_token;
					}
				}
				curl_setopt(
					$this->_curl_handler,
					CURLOPT_POSTFIELDS,
					$sign ? $this->signBody($data) : $data
				);
			}


			$this->_last_response = [];
			try {
				$response = curl_exec($this->_curl_handler);
			} catch (\Exception $e){
				$response = false;
			}
			if (ENV_DEBUG && $this->verbose) {
				echo date('Y/m/d H:i:s - ').$uri.PHP_EOL;
				echo curl_getinfo($this->_curl_handler, CURLINFO_HEADER_OUT).PHP_EOL;
				var_dump($data);
				echo $response.PHP_EOL;
			}

			if ($this->_parse_response){
				$catched = null;
				try {
					$this->processResponse($response);
				} catch (ConnectionException $e) {
					$catched = $e;
					Core::log('Connection error. Ожидание 5 минут. Попытка '.$try);
					sleep(300);
					if ($try >= 3){
						$try = 0;
						Core::$app->api->changeProxy();
					}
				} catch (ParseException $e) {
					$catched = $e;
					sleep(1);
				} catch (TooManyRequestsException $e) {
					$catched = $e;
					sleep(mt_rand(15,75));
				} catch (ServerErrorException $e) {
					$catched = $e;
					Core::log('Server error. Ожидание 10 минут');
					sleep(600);
				} catch (LoginReqiredException $e) {
					$catched = $e;
					sleep(1);
					$this->accountsLogin();
				} catch (CheckpointReqiredException $e){
					$catched = $e;
					sleep(1);
					if (!is_null(Core::$app->captcha_apikey)) {
						if ($this->recognizeCaptcha($this->_last_response['checkpoint_url'])) {
							$catched = null;
						}
						$this->_parse_response = true;
					}
				}
			} else {
				$this->_last_response = $response;
				return;
			}
		} while (!is_null($catched) && $try++ < 4);

		if (!is_null($catched)) {
			throw $catched;
		}
	}

	protected function request($uri, $data = null, $sign = true)
	{
		$this->requestRaw(self::API_URL . $uri, $data, $sign);
	}

	/**
	 * Parse instagram response and check it vor valid
	 *
	 * @param string $response
	 *
	 * @throws CheckpointReqiredException
	 * @throws ConnectionException
	 * @throws FeedbackReqiredException
	 * @throws LoginReqiredException
	 * @throws ParseException
	 * @throws ResponseException
	 * @throws ServerErrorException
	 * @throws TooManyRequestsException
	 */
	protected function processResponse($response)
	{
		if (!$response) {
			throw new ConnectionException($this->getCurlError());
		}

		$header_len = curl_getinfo($this->_curl_handler, CURLINFO_HEADER_SIZE);
		if (substr($response, $header_len - 4, 4) !== "\r\n\r\n") {
			$response   = substr($response, strpos($response, "\r\n\r\n") + 4);
		}

		$header = substr($response, 0, $header_len);
		$body   = substr($response, $header_len);

		$this->setCsrfToken($header);
		$this->_last_response = json_decode($body, true, 512, JSON_BIGINT_AS_STRING);

		if ($this->_last_response === null) {
			$body = stripslashes($body);
			$this->_last_response = json_decode($body, true, 512, JSON_BIGINT_AS_STRING);
		}

		if ($this->_last_response === null || !isset($this->_last_response['status'])) {
			throw new ParseException('Can not parse response.' . PHP_EOL . ($this->verbose ? strip_tags($body) : ''));
		}

		if ($this->_last_response['status'] !== 'ok' || isset($this->_last_response['errors'])) {
			if (isset($this->_last_response['message']) && $this->_last_response['message'] === 'login_required') {
				throw new LoginReqiredException($this->_last_response['message']);
			}
			if (isset($this->_last_response['message']) && $this->_last_response['message'] === 'feedback_required') {
				throw new FeedbackReqiredException($this->_last_response['message']);
			}
			if (isset($this->_last_response['message']) && $this->_last_response['message'] === 'checkpoint_required') {
				throw new CheckpointReqiredException($this->_last_response['message']);
			}
			//TODO: сделать перевод на разные языки девайсов
			if (isset($this->_last_response['message']) && $this->_last_response['message'] === 'Sorry, too many requests. Please try again later.') {
				throw new TooManyRequestsException($this->_last_response['message']);
			}
			if (isset($this->_last_response['message']) && $this->_last_response['message'] === 'Server error') {
				throw new ServerErrorException($this->_last_response['message']);
			}
			throw new ResponseException(sprintf('Instagram status - %s: %s',
				$this->_last_response['status'],
				isset($this->_last_response['message']) ? $this->_last_response['message'] : 'Unknown error.'
			));
		}
	}

	/**
	 * Sets $csrf_token value if finds it
	 *
	 * @param $header
	 */
	protected function setCsrfToken($header)
	{
		if (preg_match('#Set-Cookie: csrftoken=([^;]+)#', $header, $matches)) {
			$this->_csrf_token = $matches[1];
		}
	}

	/**
	 * Returns data array as string signed by HMAC SHA256 hash
	 *
	 * @param $data array
	 * @return string
	 * @throws ParamsException
	 */
	protected function signBody($data)
	{
		$dataJSON = json_encode($data);
		$hash = hash_hmac('sha256', $dataJSON, $this::API_SECRET);
//		if ($dataJSON === null){
//			throw new ParamsException(json_last_error_msg());
//		}
		return 'ig_sig_key_version=4&signed_body=' . $hash . '.' . urlencode($dataJSON);
	}

	/**
	 * Returns last cURL error as formatted string
	 *
	 * @return string
	 */
	protected function getCurlError()
	{
		return sprintf('cURL error #%s: %s',
			curl_errno($this->_curl_handler),
			curl_error($this->_curl_handler)
		);
	}

	/**
	 * Returns User-Agent header value
	 *
	 * @return string
	 */
	public function getUserAgent()
	{
		return sprintf(
			'Instagram %s %s',
			$this::API_VERSION,
			$this->device->userAgent
		);
	}
	/**
	 * Returns response from last request
	 *
	 * @return array
	 */
	public function getLastResponse()
	{
		return $this->_last_response;
	}

	public function clearCookieFile(){
		@unlink($this->_cookieFile);
	}

	public function setVerbose($value){
		$this->verbose = $value;
	}

	public function reinitCurl(){
		$this->initCurl();
	}
}