<?php namespace core\instagram\api;

use core\instagram\exceptions\AuthException;
use core\instagram\exceptions\ConnectionException;
use core\instagram\exceptions\LoginReqiredException;
use core\instagram\exceptions\ParamsException;
use core\instagram\exceptions\ParseException;
use core\instagram\exceptions\ResponseException;
use Core;

class Api_v_7_9_0 extends AbstractApi
{
	const API_SECRET = '4d53d9e2da15fd018e591939cd9689a8e7bbed50841fcb35b976ce63461cb834';
	const API_VERSION = '7.9.0';

	protected $_login_attemp = 0;

	public $changeProxyFunction;

	public function feedUser($insta_id, $max_id = null)
	{
		$url = 'feed/user/' . $insta_id . '/';

		if (!empty($max_id)) {
			$url .= '?max_id=' . $max_id;
		}
		$this->request($url);
		return $this->_last_response;
	}

	public function feedTag($tag, $count, $max_id = null)
	{
		$query = [
			//TODO: сохранять rank_token для того же запроса(тот же пользователь), но другой max_id
			'rank_token' => $this->generateUuid(),
			'count'      => $count
		];
		if ($max_id !== null) {
			$query['max_id'] = $max_id;
		}
		$this->request(sprintf('feed/tag/%s/?' . http_build_query($query), urlencode($tag)));
		return $this->_last_response;
	}

	public function feedLocation($location_id, $count = 100, $max_id = null)
	{
		$query = [
			//TODO: сохранять rank_token для того же запроса(тот же пользователь), но другой max_id
			'rank_token' => $this->generateUuid(),
			'count'      => $count
		];
		if ($max_id !== null) {
			$query['max_id'] = $max_id;
		}
		$this->request(sprintf('feed/location/%s/?' . http_build_query($query), $location_id));
		return $this->_last_response;
	}

	public function locationSearch($lat,$lng)
	{
		$query = [
			'rank_token' => $this->generateUuid(),
			'longitude'  => $lat,
			'latitude'   => $lng
		];
		$this->request('location_search/?' . http_build_query($query));
		return $this->_last_response;
	}

	public function mediaLike($media_id)
	{
		$data = [
			'media_id' => $media_id,
			'_uid'     => $this->user_id,
			'_uuid'    => $this->device->guid
		];
		$this->request('media/' . $media_id . '/like/', $data);
	}

	public function friendshipShow($insta_id)
	{
		$this->request('friendships/show/' . $insta_id . '/');
		return $this->_last_response;
	}

	public function friendshipFollowing($insta_id, $max_id = null)
	{
		$url = 'friendships/' . $insta_id . '/following/';

		if ($max_id !== null) {
			$url .= '?max_id=' . $max_id;
		}
		$this->request($url);

		return $this->_last_response;
	}

	public function friendshipFollowers($insta_id, $max_id = null)
	{

		$url = 'friendships/' . $insta_id . '/followers/';

		if ($max_id !== null) {
			$url .= '?max_id=' . $max_id;
		}
		$this->request($url);
		return $this->_last_response;
	}

	public function mediaLikers($media_id)
	{
		$this->request('media/' . $media_id . '/likers');
		return $this->_last_response;
	}

	public function usersInfo($insta_id)
	{
		$this->request('users/' . $insta_id . '/info/');
		return $this->_last_response;
	}

	public function mediaInfo($media_id)
	{
		$this->request('media/' . $media_id . '/info/');
		return $this->_last_response;
	}

	public function usersSearch($query)
	{
		$data = [
			'q'          => $query,
			'rank_token' => $this->user_id.'_'.$this->generateUuid(),
			'count'      => 50,
		];
		$this->request('users/search?'.http_build_query($data));
		return $this->_last_response;
	}

	public function friendshipsDestroy($user_id)
	{
		$data = [
			'user_id' => $user_id,
			'_uid'    => $this->user_id,
			'_uuid'   => $this->device->guid
		];
		$this->request('friendships/destroy/' . $user_id . '/', $data);
		return $this->_last_response;
	}

	public function friendshipsBlock($user_id)
	{
		$data = [
			'user_id' => $user_id,
			'_uid'    => $this->user_id,
			'_uuid'   => $this->device->guid
		];
		$this->request('friendships/block/' . $user_id . '/', $data);
		return $this->_last_response;
	}

	public function friendshipsUnblock($user_id)
	{
		$data = [
			'user_id' => $user_id,
			'_uid'    => $this->user_id,
			'_uuid'   => $this->device->guid
		];
		$this->request('friendships/unblock/' . $user_id . '/', $data);
		return $this->_last_response;
	}

	public function friendshipsCreate($user_id)
	{
		$data = [
			'user_id' => $user_id,
			'_uid'    => $this->user_id,
			'_uuid'   => $this->device->guid
		];
		$this->request('friendships/create/' . $user_id . '/', $data);
		return $this->_last_response;
	}

	public function mediaComment($media_id, $comment)
	{
		$data = [
			'comment_text'      => $comment,
			'_uid'              => $this->user_id,
			'_uuid'             => $this->device->guid,
			'idempotence_token' => $this->generateUuid()
		];
		$this->request('media/' . $media_id . '/comment/', $data);
		return $this->_last_response;
	}

	public function mediaComments($media_id, $max_id = null)
	{
		$data = [
			'media_id'          => $media_id,
			'_uid'              => $this->user_id,
			'_uuid'             => $this->device->guid,
			'max_id'            => $max_id
		];
		$this->request('media/' . $media_id . '/comments/', $data);
		return $this->_last_response;
	}

	public function mediaCommentDelete($media_id, $comment_id)
	{
		$data = [
			'comment_id'        => $comment_id,
			'_uid'              => $this->user_id,
			'_uuid'             => $this->device->guid
		];
		$this->request('media/' . $media_id . '/comment/' . $comment_id . '/delete/',$data);
		return $this->_last_response;
	}

	public function fbsearchPlaces($insta_id, $location, $limit = 20, $lat = null, $lng = null)
	{

		$query = [
			'count'        => $limit,
			'query'        => $location,
			'rank_token'   => $insta_id . '_' . $this->generateUuid(),
			'exclude_list' => ''
		];
		if ($lat !== null) {
			$query['lat'] = $lat;
		}
		if ($lng !== null) {
			$query['lng'] = $lng;
		}
		$this->request('fbsearch/places/?' . http_build_query($query));
		return $this->_last_response;
	}

	public function findBylogin($login)
	{
		$response = $this->usersSearch($login);

		if (empty($response['num_results'])) {
			return null;
		}

		foreach ($response['users'] as $user) {
			if ($user['username'] === $login) {
				return $user;
			}
		}

		return null;
	}

	public function login($username, $password)
	{
		if ($this->username != $username || $this->password != $password){
			$this->logout();
		}
		if (!$this->logged_in) {
			$this->username = $username;
			$this->password = $password;

			$this->handshake();
			$this->accountsLogin();
			$this->logged_in = true;
		}
		$this->feedTimeline();
		$result = $this->usersInfo($this->user_id);
		return $result;
	}

	public function logout()
	{
		$this->accountsLogout();
		$this->logged_in = false;
	}

	/**
	 * @param $media_id
	 * @return bool
	 */
	public function putLike($media_id)
	{
		$this->mediaLike($media_id);
		return $this->_last_response['status'] === 'ok';
	}

	/**
	 * @param $insta_id
	 * @param null $max_id
	 * @return mixed
	 */
	public function getUserFeed($insta_id, $max_id = null)
	{
		if (empty($max_id)) {
			$this->friendshipShow($insta_id);
			$this->usersInfo($insta_id);
		}
		$this->feedUser($insta_id, $max_id);
		return $this->_last_response;
	}

	/**
	 * Process image by file name, post it and sign if comment not empty
	 *
	 * @param string $file_name
	 * @param string|null $comment
	 * @param array $media_info
	 * @return bool
	 * @throws ParamsException
	 */
	public function postImage($file_name, $comment = null, $media_info = [])
	{
		$upload_id = $this->uploadPhoto($file_name);
		$this->friendshipFollowing($this->user_id);
		if (is_string($comment)) {
			sleep(rand(0, round(strlen($comment)) / 2));
		}
//		$locations = $this->locationSearch('54.8108513','83.093143');
		$locations = [];
		$media_id = $this->mediaConfigure($file_name, $upload_id, $comment, $locations, $media_info);
		sleep(rand(3, 5));
		Core::log('Id загруженного медиа '.$media_id.'. User_id: '.$this->user_id. '. '.Core::$app->api->_cookieFile);
		return ['result' => $this->checkMediaInFeed($media_id), 'media_id' => $media_id];
	}

	public function makeRandomLike()
	{
		Core::log('Делаю случайный лайк');
		$result = $this->discoverPopular()['items'];
		$media_id = $result[rand(0, count($result) - 1)]['id'];
		Core::log('Найдено медиа ' . $media_id);
		sleep(rand(1, 3));
		$this->putLike($media_id);
	}

	public function makeDownloadRequest($url)
	{
		$this->_parse_response = false;
		$this->request($url);
		$this->_parse_response = true;
		return $this->_last_response;
	}

	protected function handshake()
	{
		$this->request('si/fetch_headers/?challenge_type=signup&guid=' . $this->device->getGuid(true));
	}

	protected function accountsLogin()
	{
		$data = [
			'_csrftoken'         => $this->_csrf_token,
			'username'           => $this->username,
			'guid'               => $this->device->getGuid(),
			'device_id'          => $this->device->id,
			'password'           => $this->password,
			'login_attemp_count' => $this->_login_attemp++,
		];

		try {
			$this->request('accounts/login/', $data);
		} catch (ResponseException $e) {
			$this->logout();
			throw new AuthException('Authentication failed');
		}

		$this->user_id = $this->_last_response['logged_in_user']['pk'];
		return $this->_last_response;
	}

	protected function accountsLogout()
	{
		// TODO: Implement accountsLogout() method.
	}

	protected function qeExpose($data)
	{
		// TODO: Implement qeExpose() method.
	}

	protected function pushRegister($data)
	{
		// TODO: Implement pushRegister() method.
	}

	protected function directShareRecentRecipients($data)
	{
		// TODO: Implement directShareRecentRecipients() method.
	}

	protected function directV2Inbox($data)
	{
		// TODO: Implement directV2Inbox() method.
	}

	protected function feedTimeline($max_id = null)
	{
		$this->request('feed/timeline/');
		return $this->_last_response;
	}

	protected function news()
	{
		// TODO: Implement news() method.
	}

	public function newsInbox()
	{
		$this->request('news/inbox/');
		return $this->_last_response;
	}

	public function getExplore()
	{
		$data = [
			'people_teaser_supported' => 1,
			'rank_token'              => $this->generateUuid(),
			'no_explore_people'       => 0
		];
		$this->request('discover/popular/?' . http_build_query($data));
		return $this->_last_response;
	}

	/**
	 * @param $file_name
	 * @return mixed
	 * @throws ConnectionException
	 * @throws LoginReqiredException
	 * @throws ParseException
	 */
	protected function uploadPhoto($file_name)
	{
		$upload_id = round(microtime(true) * 1000);
		$data = [
			'upload_id' => $upload_id,
			'_uuid' => $this->device->guid,
			'image_compression' => json_encode(['lib_name' => 'jt', 'lib_version' => '1.3.0', 'quality' => '70']),
			'photo'     => new \CURLFile(
				$file_name,
				'application/octet-stream',
				'pending_media_' . round(microtime(true) * 1000) . '.jpg'
			)
		];
		$this->request('upload/photo/', $data, false);
		return $this->_last_response['upload_id'];
	}

	/**
	 * Configure media and returns it ID
	 *
	 * @param $file_name
	 * @param $upload_id
	 * @param $comment
	 *
	 * @param $locations
	 * @param $media_info
	 * @return string
	 * @throws ParamsException
	 */
	protected function mediaConfigure($file_name, $upload_id, $comment, $locations, $media_info)
	{
		if (empty($media_info['size'])){
			list($width, $height) = getimagesize($file_name);
		} else {
			list($width, $height) = $media_info['size'];
		}
		if (empty($media_info['zoom'])){
			$zoom = 1;
		} else {
			$zoom = $media_info['zoom'];
		}

		if (empty($media_info['crop'])){
			list($crop_x, $crop_y) = [0,0];
		} else {
			list($crop_x, $crop_y) = $media_info['crop'];
		}

		$data = [
			'source_type' => 4,
			'_uid'        => $this->user_id,
			'_uuid'       => $this->device->guid,
		];
		if (!empty($comment)) {
			if (!is_string($comment)) {
				throw new ParamsException('Comment must be a string');
			}
			$data['caption'] = $comment;
		}
		$data['upload_id'] = $upload_id;
		$data['device'] = [
			'manufacturer'    => $this->device->vendor,
			'model'           => $this->device->model,
			'android_version' => $this->device->os->api_level,
			'android_release' => $this->device->os->version
		];
		$data['edits'] = [
			'crop_original_size' => [$width, $height],
			'crop_center'        => [$crop_x, $crop_y],
			'crop_zoom'          => $zoom,
//			'filter_type'        => 0,
//			'filter_strength'    => 0
		];
		$data['extra'] = [
			'source_width'  => $width,
			'source_height' => $height
		];

//		$data['foursquare_request_id'] = $locations['request_id'];
//		$location = $locations['venues'][0];
//		$data['location'] = json_encode([
//			'name'    => $location['name'],
//			'address' => $location['address'],
//			'lat'     => $location['lat'],
//			'lng'     => $location['lng'],
//			'external_source' => $location['external_id_source'],
//			$location['external_id_source'].'_id' => $location['external_id']
//		]);
//		$data['suggested_venue_position'] = 0;
//		$data['is_suggested_venue'] = 1;

		$this->request('media/configure/', $data);

		return $this->_last_response['media']['pk'];
	}

	protected function friendshipsAutocompleteUserList()
	{
		$this->request('friendships/autocomplete_user_list/?followinfo=True&version=2');
		return $this->_last_response;
	}

	protected function discoverPopular()
	{
		$data = [
			'people_teaser_supported' => 1,
			'rank_token'              => $this->generateUuid(),
			'no_explore_people'       => 1
		];
		$this->request('discover/popular/?' . http_build_query($data));
		return $this->_last_response;
	}

	/**
	 * Check image, place watermark on it and make it sqared
	 *
	 * @param $file_name
	 * @throws ParamsException
	 */
	protected function processImage($file_name)
	{
		$imageInfo = getimagesize($file_name);
		if ($imageInfo === false) {
			throw new ParamsException('Can\'t get image info for '.$file_name);
		}

		list($width, $height) = $imageInfo;

		$imageResource = imagecreatefromjpeg($file_name);
		$this->placeWatermark($imageResource, $width, $height);

		if ($width !== $height) {
			Core::log('Изображение не квадратное(' . $width . 'x' . $height . '). обрезаем');
			$this->squareImage($imageResource, $width, $height);
		}

		imagejpeg($imageResource, $file_name, mt_rand(80, 99));
		imagedestroy($imageResource);
	}

	/**
	 * Place watermark on image
	 *
	 * @param $imageResource
	 * @param $width
	 * @param $height
	 */
	protected function placeWatermark(&$imageResource, $width, $height)
	{
		$watermark = imagecreatetruecolor(1, 1);
		imagefill($watermark, 0, 0, imagecolorallocate(
			$watermark,
			mt_rand(0, 255),
			mt_rand(0, 255),
			mt_rand(0, 255)
		));
		imagecopymerge($imageResource, $watermark, mt_rand(0, $width - 1), mt_rand(0, $height - 1), 0, 0, 1, 1, mt_rand(1, 10));
		imagedestroy($watermark);
	}

	/**
	 * Make image suared
	 *
	 * @param $imageResource
	 * @param $width
	 * @param $height
	 */
	protected function squareImage(&$imageResource, $width, $height)
	{
		$newSize = $width > $height ? $height : $width;
		$image_p = imagecreatetruecolor($newSize, $newSize);
		imagecopyresampled(
			$image_p,
			$imageResource,
			0, 0,
			round(($width - $newSize) / 2), round(($height - $newSize) / 2),
			$newSize, $newSize,
			$newSize, $newSize
		);
		imagedestroy($imageResource);
		$imageResource = $image_p;
	}

	/**
	 * Generates UUIDv4
	 *
	 * @return string
	 */
	protected function generateUuid()
	{
		return sprintf(
			'%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			// 32 bits for "time_low"
			mt_rand(0, 0xffff), mt_rand(0, 0xffff),
			// 16 bits for "time_mid"
			mt_rand(0, 0xffff),
			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 4
			mt_rand(0, 0x0fff) | 0x4000,
			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			mt_rand(0, 0x3fff) | 0x8000,
			// 48 bits for "node"
			mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
		);
	}

	/**
	 * @param $media_id
	 * @return bool
	 */
	private function checkMediaInFeed($media_id)
	{
		$result = $this->getUserFeed($this->user_id);
		foreach ($result['items'] as $item) {
			if ($item['pk'] === $media_id) {
				return true;
			}
		}
		return false;
	}

	public function recognizeCaptcha($url)
	{
		//стоит учитывать, что в результатах так же идут заголовки, так как ответ не парсится, а просто записывается.
		$tryes = 0;
		$this->_parse_response = false;
		do {
			$tryes++;
			Core::log('Получена капча');
//			if (!preg_match('#(http(s)?://(i\.)?instagram\.com)/integrity/checkpoint/#ui', $url, $matches)){
//				Core::log($url.' не капча');
//				return false;
//			}
//			$domain = $matches[1];
//			Core::log('Captcha domain: '.$domain);
			$this->requestRaw($url);
			$html       = $this->_last_response;
			$script_source = $this->getShortestTextBetweenNeedles('"https://www.google.com/recaptcha/api/challenge?k=', '"', $html);
			if (!$script_source) {
				Core::log('Не найдено изображение капчи. Не тот чекпоинт');
				return false;
			}
			$this->requestRaw("https://www.google.com/recaptcha/api/challenge?k=".$script_source);
			$image_url   = $this->getShortestTextBetweenNeedles("challenge : '", "'", $this->_last_response);
			if (!$image_url) {
				Core::log('Не найдено изображение по гугл ссылке '."https://www.google.com/recaptcha/api/challenge?k=".$script_source);
				continue;
			}
			//обновляем перед получением.
			$this->requestRaw('https://www.google.com/recaptcha/api/reload?type=image&c='.$image_url.'&k='.$script_source);
			$result_reload   = $this->_last_response;
			$challenge_field = $this->getShortestTextBetweenNeedles('(\'', '\'', $result_reload);
			$this->requestRaw('https://www.google.com/recaptcha/api/image?c='.$challenge_field);
			$image_code = $this->_last_response;

			$header_len = curl_getinfo($this->_curl_handler, CURLINFO_HEADER_SIZE);
			//прокси
			if (substr($image_code, $header_len - 4, 4) !== "\r\n\r\n") {
				$image_code   = substr($image_code, strpos($image_code, "\r\n\r\n") + 4);
			}

			$image_path = Core::$app->work_dir.'captcha.jpg';
			file_put_contents($image_path, substr($image_code, $header_len));
			$captcha = $this->recognizeAnticaptcha($image_path, Core::$app->captcha_apikey);
			@unlink($image_path);
			if (!$captcha) {
				Core::log('Не получен текст разгаданной капчи');
				continue;
			}
			Core::log('Получен текст: '.$captcha);
			sleep(mt_rand(5, 15));
			$token     = $this->getShortestTextBetweenNeedles('<input type="hidden" name="csrfmiddlewaretoken" value="', '"', $html);
			$post_data = http_build_query([
				'recaptcha_challenge_field' => $challenge_field,
				'csrfmiddlewaretoken'       => $token,
				'recaptcha_response_field'  => $captcha,
			]);
			Core::log('Отправка капчи');
			$send_tries = 0;

			$this->referrer = $url;
			$this->additionalHeaders['accept'] = 'text/html,application/xhtml+xml,application/xml';
			$this->reinitCurl();

			$this->requestRaw($url, $post_data, false);
			while (curl_getinfo($this->_curl_handler, CURLINFO_EFFECTIVE_URL) == $url && $send_tries < 4) {
				Core::log('Ошибка отправки капчи '.$this->getShortestTextBetweenNeedles('<p class="alert-red">', '</p>', $this->_last_response));
				$this->requestRaw($url, $post_data, false);
				$send_tries++;
			}
			if ($send_tries < 3) {
				Core::log('Капча успешно решена');
				sleep(mt_rand(15, 30));
				$this->reinitCurl();
				return true;
			}
			Core::log('Превышены попытки отправки капчи');
//			$this->changeProxy();
		} while ($tryes < 5);
		Core::log('Ошибка отправки капчи. Пауза 5-10 мин');
		sleep(mt_rand(300, 600));
		$this->reinitCurl();
		return false;
//		$token     = $this->getShortestTextBetweenNeedles('<input type="hidden" name="csrfmiddlewaretoken" value="', '"', $html);
//		$post_data = [
//			'recaptcha_challenge_field' => $image_src,
//			'csrfmiddlewaretoken'       => $this->_csrf_token,
//			'recaptcha_response_field'  => $captcha,
//		];
	}

	private function recognizeAnticaptcha($filename, $apikey)
	{
		$maxtimeout = 120;
		if (!file_exists($filename)) {
			Core::log('Captcha: no image file found: '.$filename);
			return false;
		}
		$postdata = [
			'method'     => 'post',
			'key'        => $apikey,
			'file'       => new \CURLFile($filename),
			'phrase'     => 1,
			'is_russian' => 0,
			'regsense'   => 0,
			'numeric'    => 0,
			'min_len'    => 0,
			'max_len'    => 0,

		];
		$ch       = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://anti-captcha.com/in.php");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			Core::log('Captcha: CURL returned error: '.curl_error($ch));
			return false;
		}
		curl_close($ch);
		if (stripos($result, "error") !== false) {
			Core::log('Captcha: recognize error: '.$result);
			return false;
		} else {
			$ex         = explode("|", $result);
			$captcha_id = $ex[1];
			$waittime = 0;
			$rtimeout = mt_rand(4,7);
			sleep($rtimeout);
			while (true) {
				$result = file_get_contents("http://anti-captcha.com/res.php?key=".$apikey.'&action=get&id='.$captcha_id);
				if (stripos($result, 'error') !== false) {
					Core::log('Captcha: recognize error: '.$result);
					return false;
				}
				if ($result == "CAPCHA_NOT_READY") {
					$waittime += $rtimeout;
					if ($waittime > $maxtimeout) {
						Core::log('Captcha: timelimit '.$maxtimeout);
						break;
					}
					sleep($rtimeout);
				} else {
					Core::log('Captcha success recognize');
					$ex = explode('|', $result);
					if (trim($ex[0]) == 'OK') {
						return trim($ex[1]);
					}
				}
			}
			return false;
		}
	}

	private function getShortestTextBetweenNeedles($needle_1, $needle_2, $text){
		preg_match('#'.preg_quote($needle_1).'(.*?)'.preg_quote($needle_2).'#i', $text, $element);
		if (empty($element)) {
			return false;
		}
		return isset($element[1]) ? $element[1] : $element[0];
	}

	public function changeProxy()
	{
		call_user_func($this->changeProxyFunction);
	}
}