<?php namespace core\instagram\device;

use core\base\Model;

/**
 * @property integer $width
 * @property integer $height
 * @property integer $dpi
 */

class Screen extends Model
{
	protected $_attributes = [
		'width'  => null,
		'height' => null,
		'dpi'    => null,
	];
}