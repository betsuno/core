<?php namespace core\instagram\device;

use core\base\Model;

/**
 * @property string  $family
 * @property integer $api_level
 * @property string  $version
 * @property string  $language
 */

class OS extends Model
{
	protected $_attributes = [
		'family'    => 'Android',
		'api_level' => null,
		'version'   => null,
		'language'  => 'en_US',
	];
}