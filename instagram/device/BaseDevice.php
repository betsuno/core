<?php namespace core\instagram\device;

use core\base\Model;

/**
 * @property string $vendor
 * @property string $model
 * @property string $platform
 * @property string $cpu
 * @property string $id
 * @property string $guid
 *
 * @property string $userAgent
 *
 * @property OS     $os
 * @property Screen $screen
 */

class BaseDevice extends Model
{
	protected $_attributes = [
		'os'       => null,
		'screen'   => null,
		'vendor'   => null,
		'model'    => null,
		'platform' => null,
		'cpu'      => null,
		'guid'     => null,
	];

	protected $_required_attribures = [
		'os', 'screen', 'vendor', 'model', 'platform', 'cpu'
	];

	public static function classes()
	{
		return [
			'os'     => OS::className(),
			'screen' => Screen::className(),
		];
	}

	public function getUserAgent()
	{
		return sprintf(
			'%s (%d/%s; %ddpi; %dx%d; %s; %s; %s; %s; %s)',
			$this->os->family,
			$this->os->api_level,
			$this->os->version,
			$this->screen->dpi,
			$this->screen->width,
			$this->screen->height,
			$this->vendor,
			$this->model,
			$this->platform,
			$this->cpu,
			$this->os->language
		);
	}

	/**
	 * Generates random GUID string
	 *
	 * @return string
	 */
	private function generateGuid()
	{
		return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			mt_rand(0,      0xFFFF),
			mt_rand(0,      0xFFFF),
			mt_rand(0,      0xFFFF),
			mt_rand(0x4000, 0x5000),
			mt_rand(0x8000, 0xBFFF),
			mt_rand(0,      0xFFFF),
			mt_rand(0,      0xFFFF),
			mt_rand(0,      0xFFFF)
		);
	}

	/**
	 * Returns GUID, generates it if it's not defined
	 * If the first parameter is 'true' removes dashes from the result
	 *
	 * @param bool $removeDashes
	 * @return string
	 */
	public function getGuid($removeDashes = false)
	{
		if (is_null($this->guid)) {
			$this->guid = $this->generateGuid();
		}
		return $removeDashes ? str_replace('-', '', $this->guid) : $this->guid;
	}

	/**
	 * Generates device ID that will be same for the same string
	 *
	 * @return string
	 */
	private function generateId()
	{
		return sprintf(
			'android-%04x%04x%04x%04x',
			mt_rand(0, 0xFFFF),
			mt_rand(0, 0xFFFF),
			mt_rand(0, 0xFFFF),
			mt_rand(0, 0xFFFF)
		);
	}

	public function getId()
	{
		return $this->generateId();
	}
}