<?php

use core\base\Application;
use core\base\Schedule;
class Core
{
	private static $_is_inited = false;
	/**
	 * @var Application $app
	 */
	public static $app;
	public static $db;
	public static $settings;

	private static $log_callback;

	const LOG_LEVEL_DEBUG   = 0;
	const LOG_LEVEL_INFO    = 1;
	const LOG_LEVEL_WARNING = 2;
	const LOG_LEVEL_ERROR   = 3;

	const LOG_DEST_LOCAL  = 0;
	const LOG_DEST_GLOBAL = 1;

	public static function init($config)
	{
		self::$log_callback = 'self::log_callback';

		if (self::$_is_inited) {
			return;
		}

		foreach (self::classes() as $prop => $class) {
			self::$$prop = new $class(isset($config[$prop]) ? $config[$prop] : []);
		}

		self::setEnv(empty($config['env']) ? 'release' : $config['env']);

		self::$_is_inited = true;

		self::$app->schedule = new Schedule();
	}

	private static function classes()
	{
		return [
			'app' => Application::className(),
		];
	}

	public static function log($string, $level = self::LOG_LEVEL_INFO, $destination = self::LOG_DEST_LOCAL)
	{
		call_user_func(self::$log_callback, $string, $level, $destination);
	}

	public static function debug($string, $destination = self::LOG_DEST_LOCAL)
	{
		self::log($string, self::LOG_LEVEL_DEBUG, $destination);
	}

	public static function info($string, $destination = self::LOG_DEST_LOCAL)
	{
		self::log($string, self::LOG_LEVEL_INFO, $destination);
	}

	public static function warning($string, $destination = self::LOG_DEST_LOCAL)
	{
		self::log($string, self::LOG_LEVEL_WARNING, $destination);
	}

	public static function error($string, $destination = self::LOG_DEST_LOCAL)
	{
		self::log($string, self::LOG_LEVEL_ERROR, $destination);
	}

	/**
	 * @param $callback
	 * @return bool
	 */
	public static function setLogCallback($callback)
	{
		if (!is_callable($callback)) {
				return false;
		}
		self::$log_callback = $callback;
		return true;
	}

	public static function log_callback($string, $level, $destination)
	{
		$string = date('Y/m/d H:i:s - ').$string.PHP_EOL;
		if (substr(self::$app->work_dir, -1, 1) !== DIRECTORY_SEPARATOR){
			self::$app->work_dir .= DIRECTORY_SEPARATOR;
		}
		if (ENV_DEBUG) {
			echo $string;
		}
		file_put_contents(
			self::$app->work_dir.'log.txt',
			$string,
			FILE_APPEND
		);
	}

	public static function setEnv($value)
	{
		if (!defined('ENV_DEBUG')) {
			define('ENV_DEBUG', $value === 'debug');
		}
		if (!defined('ENV_RELEASE')) {
			define('ENV_RELEASE', $value === 'release');
		}
	}

	public static function destroy(){
		foreach (self::classes() as $prop => $class) {
			self::$$prop = null;
		}
		self::$_is_inited = false;
	}

	public static function sleep($timer){
		$end_time = time() + $timer;
		do {
			self::$app->schedule->run();
			if ($end_time < time()){
				break;
			}
			sleep(1);
		} while (true);
	}


}